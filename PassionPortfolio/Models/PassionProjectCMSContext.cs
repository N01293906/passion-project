﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace PassionPortfolio.Models
{
    public class PassionProjectCMSContext :DbContext
    {
        public PassionProjectCMSContext()
        { }
        public DbSet<User> Users { get; set; }
        public DbSet<Project> Projects { get; set; }
    }
}