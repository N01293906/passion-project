﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace PassionPortfolio.Models
{
    public class Project
    {
        [Key, ScaffoldColumn(false)]
        public int ProjectId { get; set; }
        //Getters and Setters and property decleration
        [Required, StringLength(255), Display(Name = "Title:")]
        public string ProjectTitle { get; set; }
        [Required, StringLength(255), Display(Name = "Description:")]
        public string ProjectDesc { get; set; }
        [Required, StringLength(255), Display(Name = "Project link:")]
        public string ProjectLink { get; set; }

        public virtual User user { get; set; }
    }
}