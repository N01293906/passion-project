﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace PassionPortfolio.Models
{
    public class User
    {
        [Key]
        public int UserId { get; set; }
        //User first name
        [Required, StringLength(255), Display(Name = "First Name")]
        public string UserFname { get; set; }
        //User last name
        [Required, StringLength(255), Display(Name = "Last Name")]
        public string UserLname { get; set; }
        //usre email
        [Required, StringLength(255), Display(Name = "Email")]
        public string UserEmail { get; set; }
        //User contact
        [Required, StringLength(255), Display(Name = "Contact No.")]
        public string UserContact { get; set; }

        //profile picture
        //1=>picture exists (in imgs/authors/{id}.img)
        //0=>picture doesn't exist
        //public int HasPic { get; set; }

        //Accepted image formats (jpg/jpeg/png/gif)
        //public string ImgType { get; set; }


        public virtual ICollection<Project> Projects { get; set; }
    }
}