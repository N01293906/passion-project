﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PassionPortfolio.Models.View_Models
{
    public class ProjectEdit
    {
        public ProjectEdit()
        {

        }
        //To edit Project we need list of users because of relationship between User and Project i.e; One to MAny
        public virtual Project project { get; set; }

        public IEnumerable<User> users { get; set; }
    }
}