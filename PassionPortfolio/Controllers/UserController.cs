﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.Data.Entity;
using PassionPortfolio.Models;
using System.Net;
namespace PassionPortfolio.Controllers
{
    public class UserController : Controller
    {
        private PassionProjectCMSContext db = new PassionProjectCMSContext();
        // GET: User
        public ActionResult Index()
        {
            return View(RedirectToAction("List"));
        }

        public ActionResult New()
        {

            return View();
        }
        [HttpPost]
        public ActionResult Create(string UserFname_New, string UserLname_New, string UserContact_New, string UserEmail_New)
        {
            //Standard query representation   
            string query = "insert into Users (UserFname, UserLname, UserContact, UserEmail) values (@fname, @lname, @contact, @email)";
            SqlParameter[] myparams = new SqlParameter[4];
            myparams[0] = new SqlParameter("@fname", UserFname_New);
            myparams[1] = new SqlParameter("@lname", UserLname_New);
            myparams[2] = new SqlParameter("@contact", UserContact_New);
            myparams[3] = new SqlParameter("@email", UserEmail_New);


            //putting that information into the database by running a command
            db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("List");
        }
        public ActionResult List()
        {//List of users from Users table
            string query = "select * from Users";
            IEnumerable<User> users = db.Users.SqlQuery(query);//query fire on table

            return View(users);
            
        }
        public ActionResult Edit(int? id)
        {

            if ((id == null) || (db.Users.Find(id) == null))
            {
                return HttpNotFound();
            }
            string query = "select * from Users where UserId=@id";
            SqlParameter param = new SqlParameter("@id", id);
            User myuser = db.Users.SqlQuery(query, param).FirstOrDefault();
            return View(myuser);
        }

        [HttpPost]
        public ActionResult Edit(int? id, string UserFname_New, string UserLname_New, string UserContact_New, string UserEmail_New)
        {

            if ((id == null) || (db.Users.Find(id) == null))
            {
                return HttpNotFound();
            }
            string query = "update Users set UserFname=@fname, UserLname=@lname, UserEmail=@email, UserContact=@contact where UserId=@id";
            SqlParameter[] myparams = new SqlParameter[5];
            myparams[0] = new SqlParameter("@fname", UserFname_New);
            myparams[1] = new SqlParameter("@lname", UserLname_New);
            myparams[2] = new SqlParameter("@contact", UserContact_New);
            myparams[3] = new SqlParameter("@email", UserEmail_New);
            myparams[4] = new SqlParameter("@id", id);

            db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("Details/" + id);
        }

        // GET: Users/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            User user = db.Users.Find(id);
            db.Users.Remove(user);
            db.SaveChanges();
            return RedirectToAction("List");
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

    }
}