﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.Data.Entity;
using PassionPortfolio.Models;
using System.Net;
using PassionPortfolio.Models.View_Models;

namespace PassionPortfolio.Controllers
{
    public class ProjectController : Controller
    {
        private PassionProjectCMSContext db = new PassionProjectCMSContext();
        // GET: Project
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult New()
        {
            //Connection to DB needed to grant list of users
            //ask user to pick which user developed the project

            ProjectEdit projecteditview = new ProjectEdit();
            projecteditview.users = db.Users.ToList();

            return View(projecteditview);
        }
        [HttpPost]//after submit
        public ActionResult Create(string ProjectTitle_New, string ProjectDesc_New, string ProjectLink_New, int ProjectUser_New)
        {
            //Standard query representation   
            string query = "insert into Projects (ProjectTitle, ProjectDesc, ProjectLink, user_UserId) values (@ptitle, @pdesc, @plink, @author)";
            SqlParameter[] myparams = new SqlParameter[4];
            myparams[0] = new SqlParameter("@ptitle", ProjectTitle_New);
            myparams[1] = new SqlParameter("@pdesc", ProjectDesc_New);
            myparams[2] = new SqlParameter("@plink", ProjectLink_New);
            myparams[3] = new SqlParameter("@author", ProjectUser_New);

            //putting that information into the database by running a command
            db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("List");
        }
        public ActionResult List()
        {
            string query = "select * from Projects";
            //This function needs to print out a list of the projects
            IEnumerable<Project> projects = db.Projects.SqlQuery(query);//list of project
            return View(projects);
        }
        public ActionResult Edit(int id)
        {

            ProjectEdit projecteditview = new ProjectEdit();
            projecteditview.users = db.Users.ToList();//For edit we need list of users
            projecteditview.project = db.Projects.Find(id);

            return View(projecteditview);
        }

        [HttpPost]
        public ActionResult Edit(int id, string ProjectTitle_New, string ProjectDesc_New, string ProjectLink_New, int ProjectUser_New)
        {
            //If the ID doesn't exist or the project doesn't exist
            if ((id == null) || (db.Projects.Find(id) == null))
            {
                return HttpNotFound();

            }//Way to avoid sql Injection attack
            string query = "update Projects set ProjectTitle=@ptitle, ProjectDesc=@pdesc, ProjectLink=@plink, user_UserId=@author where ProjectId=@id";
            SqlParameter[] myparams = new SqlParameter[5];
            myparams[0] = new SqlParameter("@ptitle", ProjectTitle_New);
            myparams[1] = new SqlParameter("@pdesc", ProjectDesc_New);
            myparams[2] = new SqlParameter("@plink", ProjectLink_New);
            myparams[3] = new SqlParameter("@author", ProjectUser_New);
            myparams[4] = new SqlParameter("@id", id);

            db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("Details/" + id);
        }
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Project project = db.Projects.Find(id);
            if (project == null)
            {
                return HttpNotFound();
            }
            return View(project);
        }
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Project project = db.Projects.Find(id);
            if (project == null)
            {
                return HttpNotFound();
            }
            return View(project);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if ((id == null) || (db.Projects.Find(id) == null))
            {
                return HttpNotFound();

            }

            //Delete Project
            string query = "delete from Projects where ProjectId=@id";
            SqlParameter param = new SqlParameter("@id", id);
            db.Database.ExecuteSqlCommand(query, param);
            return RedirectToAction("List");
        }
    }
}